package require pbctools
#~ mol load gro md_center_prot.gro xtc KupvsTssM3_concatenated.xtc
#~ mol load gro md_center_prot.gro xtc Kupx2_concatenated.xtc
#~ mol load gro md_center_prot.gro xtc KKvsTssM3_concatenated.xtc
#~ mol load gro md_center_prot.gro xtc KKx2_concatenated.xtc
#~ mol load gro md_center_prot.gro xtc TssLx2_concatenated.xtc
mol load gro md_center_prot.gro xtc NAME_concatenated.xtc

proc fitframes { molid seltext } {
    set origin [atomselect top "index 0 to 473" frame 0]
    #~ set ref [atomselect $molid $seltext frame 0]
    set sel [atomselect $molid $seltext]
    set all [atomselect $molid all]
    
    set n [molinfo $molid get numframes]
    for { set i 1 } { $i < $n } { incr i } {
        $sel frame $i
        $all frame $i
        $all move [measure fit $sel $origin]
        }
    return
}

# Wrap around the selection - and keep the first frame to fit on
# Selection might change, check it
# 73 atoms in Kup
# 75 atoms in KK
# 72 atoms in L
#~ pbc wrap -centersel "index 0 to 72" -center com -first 1 -last last -compound resid
#~ pbc wrap -centersel "index 0 to 74" -center com -first 1 -last last -compound resid
#~ pbc wrap -centersel "index 0 to 71" -center com -first 1 -last last -compound resid
pbc wrap -centersel "SEL1" -center com -first 1 -last last -compound resid

# Fit the selection
fitframes top "all"

# Volmap
#~ volmap occupancy [atomselect top "index 73 to 141"] -res 1.0  -allframes -combine avg -o TssM3_around_Kup.dx -checkpoint 0
#~ volmap occupancy [atomselect top "index 73 to 145"] -res 1.0  -allframes -combine avg -o Kup_around_Kup.dx -checkpoint 0
#~ volmap occupancy [atomselect top "index 75 to 143"] -res 1.0  -allframes -combine avg -o TssM3_around_KK.dx -checkpoint 0
#~ volmap occupancy [atomselect top "index 75 to 149"] -res 1.0  -allframes -combine avg -o KK_around_KK.dx -checkpoint 0
#~ volmap occupancy [atomselect top "index 72 to 143"] -res 1.0  -allframes -combine avg -o TssL_around_TssL.dx -checkpoint 0
volmap occupancy [atomselect top "SEL2"] -res 1.0  -allframes -combine avg -o NAME_volmap.dx -checkpoint 0

quit
