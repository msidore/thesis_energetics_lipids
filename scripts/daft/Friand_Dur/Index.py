#!/usr/bin/python
# -*- coding: utf-8 -*-
# Friand_Dur  Copyright Notice
# ============================
#
# Science thrives on openness. This is not a classic copyright notice.
# You may freely use, copy, modify, devastate, rape or steal any
# part of this code, with or without notification to its original owner.
#
# You MUST include a copy of this notice in all versions of any
# software that include even a tiny bit of this code. This is because
# it is science, and science needs openness, badly.
#
# ---------------------------------------------------------------------
__doc__="""
The Index class.
Process the indexes of a .ndx file.
"""

class Index_GRO:
    def __init__(self, **kwargs):
        # The index title and list
        self.name = None
        self.atom_indexes = []

        for key, val in kwargs.items():
            setattr(self, key, val)

    def store_ndx(self, ndx):
        """ Store one index title and all the numbers"""

        # ndx can be splitted into title and numbers with the remaining [
        title, inds = ndx.split("]")
        # The name is then ...
        self.name = title.strip()
        # And all the indexes
        self.atom_indexes = [int(x) for x in " ".join(inds.split("\n")).split()]

        return self
        
    def add_ndx(self, name, indx):
        """ Store an index from its name and index numbers in a list """
        
        # The name, directly
        self.name = name
        
        # And the indexes
        self.atom_indexes = indx
        
        return self

    def get_title(self):
        """ Get the name """

        return self.name
        
    def get_indic(self):
        """ Get the indexes """
        
        return self.atom_indexes
        
    def get_indtuple(self):
        """ Get the tuple (title, indexes) """
        
        return (self.name, self.atom_indexes)
