#!/usr/bin/python
# -*- coding: utf-8 -*-
# Friand_Dur  Copyright Notice
# ----------------------------
#
# Science thrives on openness. This is not a classic copyright notice.
# You may freely use, copy, modify, devastate, rape or steal any
# part of this code, with or without notification to its original owner.
#
# You MUST include a copy of this notice in all versions of any
# software that include even a tiny bit of this code. This is because
# it is science, and science needs openness, badly.
#
# ---------------------------------------------------------------------
__doc__="""
The Residue class.
Contains everything to handle one single residue, including computing all the boring stuff about what is side-chain/backbone for whatever force-field
"""

from Atom import *

class Residue():

    def __init__(self, newatom=None, **kwargs):
        # The one letter AA code
        self.name = None
        # The 3 letter AA code
        self.tname = None
        # The indexes that are inside this residue
        self.ind = []
        # The residue number
        self.resid = None
        # The atoms that are inside (as atom class)
        self.atoms = []

        # Dictionaries of the correspondance between the 3 letter AA code and 1 letter AA code
        self.tadict = {'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'GLN': 'Q', 'LYS': 'K',
     'ILE': 'I', 'PRO': 'P', 'THR': 'T', 'PHE': 'F', 'ASN': 'N',
     'GLY': 'G', 'HIS': 'H', 'LEU': 'L', 'ARG': 'R', 'TRP': 'W',
     'ALA': 'A', 'VAL':'V', 'GLU': 'E', 'TYR': 'Y', 'MET': 'M'}
        self.atdict = {v: k for k, v in self.tadict.items()}

        for key, val in kwargs.items():
            setattr(self, key, val)

        if newatom!=None:
            self.read_atom(newatom)

    def read_atom(self, atom):
        """ Read an atom to create a residue """

        # The resname
        if len(atom.resname) == 3:
            self.tname = atom.resname
            self.name = self.tadict[atom.resname]
        elif len(atom.resname) == 1:
            self.name = atom.resname
            self.tname = self.atdict[atom.resname]
        else:
            "Residue name not understood, exiting ..."
            sys.exit()

        # The residue number
        self.resid = atom.resnumber

        # Add that index to the residue index list
        self.ind.append(atom.number)

        # And finally add the atom for good measure
        self.atoms.append(atom)

        return self

    def add_atom(self, atom):
        """ Add an atom to that residue """

        self.ind.append(atom.number)
        self.atoms.append(atom)

    def get_resid(self):
        return self.resid
        
    def get_resid_name(self):
        return self.name
