# Friand_Dur  Copyright Notice
# ----------------------------
#
# Science thrives on openness. This is not a classic copyright notice.
# You may freely use, copy, modify, devastate, rape or steal any
# part of this code, with or without notification to its original owner.
#
# You MUST include a copy of this notice in all versions of any
# software that include even a tiny bit of this code. This is because
# it is science, and science needs openness, badly.
#
# ---------------------------------------------------------------------
__doc__="""
This module contains the AtomHandler class.
It's possibly the most important one, will store atom lists and coordinates.
"""

import numpy, random

class AtomHandler:
    """ Class to handle sets of atoms with lists """

    def __init__(self, **kwargs):
        """ the __init___ """
        # The basic list of atoms, starts empty
        self.atoms = []
        # Converts all the arguments called from AtomHandler(a=A, b=B), passed into kwargs {'a': 'A', 'b': 'B'} into attributes of the class
        # Calling class.a will return A
        for key, val in kwargs.items():
            setattr(self, key, val)

