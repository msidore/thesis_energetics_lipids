#!/bin/bash

#sim="TssLx2"

for sim in Lvs2K
do

name="./${sim}/"
cd $name

for i in {001..500}
do
    cd ${sim}-0$i

    # Rename the includes
    sed "s/martini.itp/\/scratch\/cnt0023\/ism6198\/sidorem\/production\/Camille\/_TM1-_TM2\/martini_v2.2.itp/g" ${sim}.top > ${sim}_mod.top
    sed -i "s/martini_v2.0_lipids.itp/\/scratch\/cnt0023\/ism6198\/sidorem\/production\/Camille\/_TM1-_TM2\/martini_v2.0_lipids.itp/g" ${sim}_mod.top
    sed -i "s/martini_v2.0_ions.itp/\/scratch\/cnt0023\/ism6198\/sidorem\/production\/Camille\/_TM1-_TM2\/martini_v2.0_ions.itp/g" ${sim}_mod.top

    cd ..
done

cd ..
done
