#!/bin/bash
module load intel/16.4.258
module load intelmpi/5.1.3.258
module load gromacs/5.1.4

for sim in Lvs2K
do

name="./${sim}/"
cd $name

for i in {001..125}
do
    cd ${sim}-0$i

    # NPT
    gmx_mpi grompp -f /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/sample_PROD.mdp -c npt.gro -p ${sim}_mod.top -o md.tpr -n ${sim}.ndx -t npt.cpt
    currentname=0$i-md
    sed "s/#NAME#/$currentname/g" /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/Tss_prod.slurm > md.slurm
    sed -i "s/#STEP#/md/g" md.slurm
    sbatch md.slurm

    cd ..
done

# Wait til they finish, check every 10s
while [ `squeue -u sidorem | wc | awk '{print $1-1}'` != 0 ]
do
sleep 10
squeue -u sidorem
done

for i in {126..250}
do
    cd ${sim}-0$i

    # NPT
    gmx_mpi grompp -f /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/sample_PROD.mdp -c npt.gro -p ${sim}_mod.top -o md.tpr -n ${sim}.ndx -t npt.cpt
    currentname=0$i-md
    sed "s/#NAME#/$currentname/g" /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/Tss_prod.slurm > md.slurm
    sed -i "s/#STEP#/md/g" md.slurm
    sbatch md.slurm

    cd ..
done

# Wait til they finish, check every 10s
while [ `squeue -u sidorem | wc | awk '{print $1-1}'` != 0 ]
do
sleep 10
squeue -u sidorem
done

for i in {251..375}
do
    cd ${sim}-0$i

    # NPT
    gmx_mpi grompp -f /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/sample_PROD.mdp -c npt.gro -p ${sim}_mod.top -o md.tpr -n ${sim}.ndx -t npt.cpt
    currentname=0$i-md
    sed "s/#NAME#/$currentname/g" /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/Tss_prod.slurm > md.slurm
    sed -i "s/#STEP#/md/g" md.slurm
    sbatch md.slurm

    cd ..
done

# Wait til they finish, check every 10s
while [ `squeue -u sidorem | wc | awk '{print $1-1}'` != 0 ]
do
sleep 10
squeue -u sidorem
done

for i in {376..500}
do
    cd ${sim}-0$i

    # NPT
    gmx_mpi grompp -f /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/sample_PROD.mdp -c npt.gro -p ${sim}_mod.top -o md.tpr -n ${sim}.ndx -t npt.cpt
    currentname=0$i-md
    sed "s/#NAME#/$currentname/g" /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/Tss_prod.slurm > md.slurm
    sed -i "s/#STEP#/md/g" md.slurm
    sbatch md.slurm

    cd ..
done

# Wait til they finish, check every 10s
while [ `squeue -u sidorem | wc | awk '{print $1-1}'` != 0 ]
do
sleep 10
squeue -u sidorem
done

cd ..
done
