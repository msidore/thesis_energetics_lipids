#!/bin/bash
module load intel/16.4.258
module load intelmpi/5.1.3.258
module load gromacs/5.1.4

for sim in Lvs2K
do

# Enter the directory
name="./${sim}/"
cd $name

# First 125 (doing 500/4)
for i in {001..125}
do
    cd ${sim}-0$i
    # NVT
    gmx_mpi grompp -f /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/sample_NVT.mdp -c em.gro -p ${sim}_mod.top -o nvt.tpr -n ${sim}.ndx
    currentname=0$i-nvt
    sed "s/#NAME#/$currentname/g" /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/Tss_equil.slurm > nvt.slurm
    sed -i "s/#STEP#/nvt/g" nvt.slurm
    sbatch nvt.slurm

    cd ..
done

# Wait til they finish, check every 10s
while [ `squeue -u sidorem | wc | awk '{print $1-1}'` != 0 ]
do
sleep 10
squeue -u sidorem
done

# Next 125
for i in {126..250}
do
    cd ${sim}-0$i
    # NVT
    gmx_mpi grompp -f /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/sample_NVT.mdp -c em.gro -p ${sim}_mod.top -o nvt.tpr -n ${sim}.ndx
    currentname=0$i-nvt
    sed "s/#NAME#/$currentname/g" /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/Tss_equil.slurm > nvt.slurm
    sed -i "s/#STEP#/nvt/g" nvt.slurm
    sbatch nvt.slurm

    cd ..
done

# Wait til they finish, check every 10s
while [ `squeue -u sidorem | wc | awk '{print $1-1}'` != 0 ]
do
sleep 10
squeue -u sidorem
done

# Next 125
for i in {251..375}
do
    cd ${sim}-0$i
    # NVT
    gmx_mpi grompp -f /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/sample_NVT.mdp -c em.gro -p ${sim}_mod.top -o nvt.tpr -n ${sim}.ndx
    currentname=0$i-nvt
    sed "s/#NAME#/$currentname/g" /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/Tss_equil.slurm > nvt.slurm
    sed -i "s/#STEP#/nvt/g" nvt.slurm
    sbatch nvt.slurm

    cd ..
done

# Wait til they finish, check every 10s
while [ `squeue -u sidorem | wc | awk '{print $1-1}'` != 0 ]
do 
sleep 10
done

# Next 125
for i in {376..500}
do
    cd ${sim}-0$i
    # NVT
    gmx_mpi grompp -f /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/sample_NVT.mdp -c em.gro -p ${sim}_mod.top -o nvt.tpr -n ${sim}.ndx
    currentname=0$i-nvt
    sed "s/#NAME#/$currentname/g" /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/Tss_equil.slurm > nvt.slurm
    sed -i "s/#STEP#/nvt/g" nvt.slurm
    sbatch nvt.slurm

    cd ..
done

# Wait til they finish, check every 10s
while [ `squeue -u sidorem | wc | awk '{print $1-1}'` != 0 ]
do
sleep 10
squeue -u sidorem
done

# Go into another directory for the next sim
cd ..
done
