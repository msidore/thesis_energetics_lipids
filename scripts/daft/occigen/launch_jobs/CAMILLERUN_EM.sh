#!/bin/bash
module load intel/16.4.258
module load intelmpi/5.1.3.258
module load gromacs/5.1.4

#sim=TssLx2

for sim in Lvs2K
do

name="./${sim}/"
cd $name

for i in {001..200}
do
    cd ${sim}-0$i
    
    # Set all angles at 90, for a rectangle box
    gmx_mpi editconf -f ${sim}.gro -o ${sim}_rect.gro -angles 90 90 90
    # EM
    gmx_mpi grompp -f /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/sample_EM.mdp -c ${sim}_rect.gro -p ${sim}_mod.top -o em.tpr
    currentname=0$i-em
    sed "s/#NAME#/$currentname/g" /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/Tss_EM.slurm > em.slurm
    sed -i "s/#STEP#/em/g" em.slurm
    sbatch em.slurm

    cd ..
done

while [ `squeue -u sidorem | wc | awk '{print $1-1}'` != 0 ]
do
sleep 20
done

for i in {201..400}
do
    cd ${sim}-0$i

    # Set all angles at 90, for a rectangle box
    gmx_mpi editconf -f ${sim}.gro -o ${sim}_rect.gro -angles 90 90 90
    # EM
    gmx_mpi grompp -f /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/sample_EM.mdp -c ${sim}_rect.gro -p ${sim}_mod.top -o em.tpr
    currentname=0$i-em
    sed "s/#NAME#/$currentname/g" /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/Tss_EM.slurm > em.slurm
    sed -i "s/#STEP#/em/g" em.slurm
    sbatch em.slurm

    cd ..
done

while [ `squeue -u sidorem | wc | awk '{print $1-1}'` != 0 ]
do
sleep 20
done

for i in {401..500}
do
    cd ${sim}-0$i

    # Set all angles at 90, for a rectangle box
    gmx_mpi editconf -f ${sim}.gro -o ${sim}_rect.gro -angles 90 90 90
    # EM
    gmx_mpi grompp -f /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/sample_EM.mdp -c ${sim}_rect.gro -p ${sim}_mod.top -o em.tpr
    currentname=0$i-em
    sed "s/#NAME#/$currentname/g" /scratch/cnt0023/ism6198/sidorem/production/Camille/_TM1-_TM2/Tss_EM.slurm > em.slurm
    sed -i "s/#STEP#/em/g" em.slurm
    sbatch em.slurm

    cd ..
done

while [ `squeue -u sidorem | wc | awk '{print $1-1}'` != 0 ]
do
sleep 20
done

cd ..
done
