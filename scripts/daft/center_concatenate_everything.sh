#!/bin/bash

for sim in TssLvsTssM3 Lvs2K KKx2 KKvsTssM3 KupvsTssM3 Kupx2 TssLx2
do

ffolder="./${sim}/"
cd $ffolder

for i in {001..500}
do

cd ${sim}-0$i

echo 2 | gmx trjconv -f md_center.gro -s md.tpr -n ${sim}.ndx -o md_center_prot.gro

cd ..
done

gmx trjcat -f ${sim}-0*/md_center_prot.gro -o ${sim}_concatenated.xtc -cat
cp ${sim}-0001/md_center_prot.gro .

cd ..
done
