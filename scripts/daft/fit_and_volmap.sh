#!/bin/bash

# To use after concatenate_everything.sh

for sim in TssLvsTssM3 Lvs2K KKx2 KKvsTssM3 KupvsTssM3 Kupx2 TssLx2
do

ffolder="./${sim}/"
cd $ffolder

# A bunch of seds
sed "s/NAME/${sim}/g" ../fit_and_volmap.tcl > fit_and_volmap_ok.tcl

# sed for the selections
# TssL first
if [ ${sim} = "TssLx2" ]
then 
sed -i "s/SEL1/index 0 to 71/g" fit_and_volmap_ok.tcl
sed -i "s/SEL2/index 72 to 143/g" fit_and_volmap_ok.tcl
elif [ ${sim} = "TssLvsTssM3" ]
then
sed -i "s/SEL1/index 0 to 71/g" fit_and_volmap_ok.tcl
sed -i "s/SEL2/index 72 to 140/g" fit_and_volmap_ok.tcl
elif [ ${sim} = "Lvs2K" ]
then
sed -i "s/SEL1/index 0 to 71/g" fit_and_volmap_ok.tcl
sed -i "s/SEL2/index 73 to 146/g" fit_and_volmap_ok.tcl
# Kup
elif [ ${sim} = "Kupx2" ]
then 
sed -i "s/SEL1/index 0 to 72/g" fit_and_volmap_ok.tcl
sed -i "s/SEL2/index 73 to 145/g" fit_and_volmap_ok.tcl
elif [ ${sim} = "KupvsTssM3" ]
then
sed -i "s/SEL1/index 0 to 72/g" fit_and_volmap_ok.tcl
sed -i "s/SEL2/index 73 to 141/g" fit_and_volmap_ok.tcl
# 2K
elif [ ${sim} = "KKx2" ]
then 
sed -i "s/SEL1/index 0 to 74/g" fit_and_volmap_ok.tcl
sed -i "s/SEL2/index 75 to 149/g" fit_and_volmap_ok.tcl
elif [ ${sim} = "KKvsTssM3" ]
then
sed -i "s/SEL1/index 0 to 74/g" fit_and_volmap_ok.tcl
sed -i "s/SEL2/index 75 to 143/g" fit_and_volmap_ok.tcl
fi

vmd_bin -dispdev text -e fit_and_volmap_ok.tcl

cd ..

done
