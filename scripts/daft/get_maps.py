#!/usr/bin/python
# -*- coding: utf-8 -*-
from argparse import ArgumentParser
import os, sys
import numpy as np
import matplotlib.pyplot as plt
# The folder where Friand Dur is located !
sys.path.append('/home/ub/bin/Friand_Dur')
from Model import Model

"""

Usage: ./get_maps.py -f TssLx2 -i 500 -n TssLx2.ndx -p

"""

######################## Parsing stuff ########################

parser = ArgumentParser(description=""" Do the contact map analysis 
/!\ The .ndx is special here, it's the one produced by DAFT which has only the two proteins /!\
""")

# Named arguments
parser.add_argument("-f", "--name", help="The generic name of the simulation", required=True)
parser.add_argument("-i", "--number", help="The number of replicates", required=True)
parser.add_argument("-n", "--index", help="The index .ndx file", required=True)

# Optional arguments
parser.add_argument("-p", "--proximity", help="Contact map not with distance but with proximity, see DAFT paper", required=False, action="store_true")
parser.add_argument("-bb", "--backbone", help="Contact map only between backbone atoms", required=False, action="store_true")

args = parser.parse_args()

######################## Functions ########################

# Get the arguments
name = args.name
ndx = args.index
n = int(args.number)

# Extract the dimer from the badly formatted ndx
def extract_dimer_bad_ndx(ndx):
    """The file has, for each molecule (so, two), a header in brackets and
    each index on a line"""
    
    # First split at the brackets [
    # Which means discarding the first member of the resulting list
    # Since the file starts with [
    ff = open(ndx, "r").read().split("[")[1:]
    
    # Also split at each ], making a list of two lists of 2 elements
    # Meaning, [[name1, indexes], [name2, indexes]]
    ff = [x.split("]") for x in ff]
    
    # Now deal with the \n
    # Remove trailing and leading \n in each sublist
    # And then split at the other \n to get a list of indexes
    # Not using too much list comprehension to not make it too hard to read
    newlist = []
    for i, sub in enumerate(ff):
        subdimer = [x.strip("\n") for x in sub]
        subdimer = [x.split("\n") for x in subdimer]
        newlist.append(subdimer)
        
    return newlist

# Calculate a distance
def calc_atom_dist(atom_one, atom_two):
    """Returns the distance between two points"""

    # Gets the vector between two residues
    diff_vector  = atom_one.coords - atom_two.coords

    # Gets the length of that vector
    vec_length = np.sqrt(np.sum(diff_vector * diff_vector))
    
    # Return the length by default or proximity if specified
    if args.proximity:
        # Proximity, as defined in DAFT, is exp(-0.01 * distance ^ (2*2))
        # I guess the distance is ^(2*2), and not the whole exponent (please)
        proximity = np.exp(-0.1 * np.power(vec_length, 4))
        return proximity
    else:
        return vec_length

# Calculate a distance matrix
def calc_dist_matrix(chain_one, chain_two):
    """Returns a matrix of distances between two chains"""

    # First create an empty (with zeros) 2d matrix
    answer = np.zeros((len(chain_one), len(chain_two)), np.float)

    # For every atom pair, gets the distance and put it into the matrix
    for row, atom_one in enumerate(chain_one) :
        for col, atom_two in enumerate(chain_two) :
            answer[row, col] = calc_atom_dist(atom_one, atom_two)

    return answer

# Cut the map
def cut_distance_matrix(distance_matrix):
    """ Returns a cut matrix """
    
    # The threshold
    threshold = 0.7
    # ~ threshold = 0.5
    
    # First create an empty matrix with zeroes of the same shape
    answer = np.zeros(distance_matrix.shape)
    
    # For each case, see if that goes below/after a threshold
    for rowid in range(len(distance_matrix)):
        for colid in range(len(distance_matrix[rowid])):
            
            # If it's below the threshold, put 0
            # ~ if distance_matrix[rowid][colid] < threshold: answer[rowid][colid] = 0.5
            if distance_matrix[rowid][colid] < threshold: answer[rowid][colid] = 0.7
            
            # Else keep it
            else: answer[rowid][colid] = distance_matrix[rowid][colid]
    
    # Return the cut matrix
    return answer

######################## Main ########################

####### Digression
"""
The idea behind all that. The .ndx is special here, it's the one produced by DAFT 
which has only the two proteins /!\ 
We will therefore just use this special index file (by hand though, since it's not
formatted how it should be) and use it to build two objects/lists corresponding
to the two proteins. 

Once we have them, with atom objects containing the coordinates, we can do stuff.
"""
####### /Digression

# A list that will contain all the distance matrices
all_the_distance_matrix = []

# Get the indexes
dimer1_ind, dimer2_ind = extract_dimer_bad_ndx(ndx)

# All the structures are in the subdirectories name/name-0xxx/
# Also, do only 10 for testing purposes
# ~ for i in range(1, n+1):
for i in range(1, 501):
    
    # Which directory are we at
    directory = "{:s}-{:04d}".format(name, i)
    
    # What's the gro file
    gro = "{:s}/{:s}/md_center.gro".format(name, directory)

    # Get all the protein atoms from it
    # Right now, my gro importer only works for CG protein atoms and ignores everything else
    # This is a list of Atom objects btw
    protein_atoms = Model(gro_protein=gro).atoms

    # Put each atoms in its protein (list) variable
    # Empty lists for a start
    # /!\ Probably wise to only keep backbone atoms, so
    dimer1, dimer2 = [], []
    for j, atom in enumerate(protein_atoms):
        
        # Discard if it's not a backbone atom
        if args.backbone:
            if atom.name != "BB": continue
        
        # Put the atom in its right chain
        if atom.number in dimer1_ind[1]:
            dimer1.append(atom)
        elif atom.number in dimer2_ind[1]:
            dimer2.append(atom)
            
    # Now we have both dimer atoms
    # Go straight to it: distance matrix between the two dimers !
    # So, get the distance matrix between the two sets of atoms
    dist_matrix = calc_dist_matrix(dimer1, dimer2)
    
    # Store it and do the next one
    all_the_distance_matrix.append(dist_matrix)

# Now that we have all the distance matrices, do something with them
# Either average or I don't know, just make a map in each subdirectory, let's do that
# Or not, average all the matrices and make a figure

# Convert the big list into a numpy array
all_the_distance_matrix = np.array(all_the_distance_matrix)

# Debug - get its shape 
# ~ print all_the_distance_matrix.shape

# Average along the 0th axis
final_distance_matrix = np.mean(all_the_distance_matrix, axis=0)

# Debug - get the shape of the final matrix
# ~ print final_distance_matrix.shape

# Cut the map, we don't care about things that don't interact
final_distance_matrix = cut_distance_matrix(final_distance_matrix)
# ~ print final_distance_matrix

# Dirty way to get the right numbering
# 33 => TssM, else it's L
if len(final_distance_matrix[0]) == 33:
    x_range = range(356, 389)
else:
    x_range = range(179, 213)

y_range = range(179, 213)

print final_distance_matrix.shape

# Matplotlib part
plt.figure()
# flipud lets us flip the matrix up and down (as we will reverse the y axis)
#~ plt.imshow(numpy.flipud(dist_matrix), interpolation='none', cmap=plt.get_cmap('hot'))
# No need to flip anything here, we'll flip later
if args.backbone:
    plt.imshow(final_distance_matrix, interpolation='none', cmap=plt.get_cmap('seismic'), vmin=0.7, vmax=1)
    # ~ plt.imshow(final_distance_matrix, interpolation='none', cmap=plt.get_cmap('seismic'), vmin=0.5, vmax=1)
else:
    plt.imshow(final_distance_matrix, interpolation='none', cmap=plt.get_cmap('seismic'), vmin=0.5, vmax=1)
plt.grid(True, color='green')
ax = plt.gca()
ax.set_xticks(range(len(final_distance_matrix[0])))
ax.set_xticklabels([str(x) for x in x_range])
ax.set_yticks(range(len(final_distance_matrix)))
# Get the numbering right
numbers = [y+1 for y in list(reversed(range(len(final_distance_matrix))))]
# [::-1] reverses a list
# ~ ax.set_yticklabels([str(numbers[x]) for x in range(len(final_distance_matrix))][::-1])
ax.set_yticklabels([str(x) for x in y_range])
# Now we're flippin'
ax.invert_yaxis()
plt.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c=".3")
# Rotates the xticks labels
plt.xticks(rotation=90)
# ~ plt.title("Average of the simulations")
# Add the colored scale
plt.colorbar()
# Saving
if args.backbone:
    plt.savefig("{:s}_rawaverage_bb.png".format(name, name), bbox_inches='tight')
else:
    plt.savefig("{:s}_rawaverage.png".format(name, name), bbox_inches='tight')
plt.close()












