#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, subprocess, os
from argparse import ArgumentParser

""" Usage:  """

######################## Parsing stuff ########################

parser = ArgumentParser(description="""   """)

# Named arguments
parser.add_argument("-f", "--xtc", help="The name of the input .xtc file.", required = True)
parser.add_argument("-p", "--pdb", help="The name of the .pdb or .gro file.", required = True)
parser.add_argument("-s", "--tpr", help="The name of the .tpr file.", required = True)
parser.add_argument("-o", "--out", help="The output name.", required = True)

# Optional arguments
parser.add_argument("-nf", "--numframes", help="The total number of frames.", required = False)
parser.add_argument("-m", "--multiplier", help="If you didn't save your trajectory every 10000 steps, add a multiplier to 10000.", required = False)

args = parser.parse_args()

######################## Functions & parser args ########################

# The named arguments
xtc = args.xtc
pdb = args.pdb
tpr = args.tpr
out_name = args.out

# A function to get the number of frames
def get_numframes(pdb, xtc):
    """Gets the number of frames"""

    import MDAnalysis
    mobile = MDAnalysis.Universe(pdb, xtc)

    return mobile.trajectory.__len__()

# The main function
def make_slice(xtc, tpr, _out, out_name, total_time, split_number, position):
    """Recursion to slice the trajectory into bits"""

    if total_time-position>split_number:
        
        # Here, if there's more than the split_number left in the trajectory, write the intermediate command
        
        # The new position is therefore one split_number later
        new_position = position + split_number
        
        # And the output name has both the old and new positions
        current_output = "{:s}_{:d}to{:d}ps.xtc".format(out_name, position, new_position)
        
        # The command is therefore
        command = gmx_call.format(xtc, tpr, position, new_position, current_output)
        
        # Write it
        _out.write(command)
        
    else:
        
        # If not, it's the last - the new position is simply num_frames
        current_output = "{:s}_{:d}to{:d}ps.xtc".format(out_name, position, total_time)
        
        # The command doesn't change much
        command = gmx_call.format(xtc, tpr, position, total_time, current_output)
        
        # Write it
        _out.write(command)
    
    # The escape button
    if split_number+position>total_time:
        _out.close()
        sys.exit()

    # Go recursive
    return make_slice(xtc, tpr, _out, out_name, total_time, split_number, new_position)

######################## Main ########################

# If the max number of frames is in the command line, use it, else go fetch it in the trajectory with MDAnalysis
if args.numframes:
    num_frames = args.numframes
else:
    num_frames = get_numframes(pdb, xtc)
    
# We don't want the actual number of frames in fact, we want the total number of ps
# Which is
total_time = num_frames * 200

# We start at time 0 (ps) and for num_frames frames, which has 10.000*20fs = 200.000fs
# = 200ps per frame
# We want to split every ... 2500 frames to be safe, since this is a big trajectory
# And 2500 frames are 2500*200ps = 500.000ps
split_number = 500000

# If we need to add a multiplier to this, in case it's a 1of40 trajectory for instance
if args.multiplier:
    multiplier = int(args.multiplier)
    total_time *= multiplier
    split_number *= multiplier

# Also, what we'll actually do: write the successive gmx calls in a sh script
gmx_call = """cat << EOF | gmx trjconv -f {:s} -s {:s} -b {:d} -e {:d} -o {:s}
0
EOF
"""

# Also, the bash file
_out = open("split_your_traj.sh", "w")

make_slice(xtc, tpr, _out, out_name, total_time, split_number, 0)












