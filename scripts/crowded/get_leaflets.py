#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, subprocess, os
from argparse import ArgumentParser
import MDAnalysis
import MDAnalysis.selections.gromacs

# Basic variables
input_filename = "/media/ub/DATA/Notebook/systems/duplicated_protlipids.gro"
z_coord = "100"
output_generic = "initial"

# Store the gro file in a Universe object
gro_universe = MDAnalysis.Universe(input_filename)

# The two leaflets are above and below the z_coord
leaflet_0 = gro_universe.select_atoms("byres ((name PO4 or name PO41) and prop z > %s)" % z_coord)
leaflet_1 = gro_universe.select_atoms("byres ((name PO4 or name PO41) and prop z < %s)" % z_coord)

# create the gro files
W0_gro = MDAnalysis.coordinates.GRO.GROWriter(output_generic+"_0.gro")
W0_gro.write(leaflet_0.residues.atoms)
print "First leaflet .gro"
print leaflet_0.residues.atoms

W1_gro = MDAnalysis.coordinates.GRO.GROWriter(output_generic+"_1.gro")
W1_gro.write(leaflet_1.residues.atoms)
print "Second leaflet .gro"
print leaflet_1.residues.atoms

# create the ndx files - useful to separate the leaflets in an xtc file
W0_ndx = MDAnalysis.selections.gromacs.SelectionWriter(output_generic+"_0.ndx")
W0_ndx.write(leaflet_0.residues.atoms)

W1_ndx = MDAnalysis.selections.gromacs.SelectionWriter(output_generic+"_1.ndx")
W1_ndx.write(leaflet_1.residues.atoms)
