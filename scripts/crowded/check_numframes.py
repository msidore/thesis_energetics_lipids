#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, subprocess, os
from argparse import ArgumentParser
import MDAnalysis

pdb = "/media/ub/DATA/Trajectories/CBol/Bol_19500000.pdb"
# ~ xtc = "/media/ub/DATA/Trajectories/CBol/meteo/filter_10.xtc"
xtc = "/media/ub/DATA/Trajectories/CBol/CBol_19500000to20000000ps.xtc"

universe = MDAnalysis.Universe(pdb, xtc)

print "The trajectory has {:d} frames".format(len(universe.trajectory))
