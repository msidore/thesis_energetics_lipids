#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, subprocess, os, numpy
from argparse import ArgumentParser
# The folder where Friand Dur is located !
sys.path.append('/home/ub/bin/Friand_Dur')
from Model import Model

""" One shot - add to a ndx file everything for the monomer of AqpZ.

Usage: ./ndx_handle.py -n CBol.ndx -o CBol_updated.ndx

"""

######################## Parsing stuff ########################

parser = ArgumentParser(description="""One shot - add to a ndx file everything for the monomer of AqpZ.""")

# Named arguments
parser.add_argument("-n", "--index", help="The name of the input index file", \
        required=True)
parser.add_argument("-o", "--output", help="The name of the output index file", \
        required=True)

args = parser.parse_args()

######################## Functions and miscellaneous ########################

# Input
index_file = args.index

# Output
output_index = args.output

# Some indexes in the AqpZ


######################## Main ########################

# Initiate the class
model = Model(ndx=index_file)

# All - the - index - titles
index_titles = model.get_index_titles()
index_numbers = model.get_indices()
len_ndx = len(index_titles)

# The protein is the index 1, so
protein_indexes = index_numbers[1]

# The .GRO is a bit messy and the monomers aren't following each other, so
# Basically, we need to go through the index numbers and decompose it 
# One AqpZ is 474 atoms, so ...
nb_AqpZ = 474

# The empty list that will have the indexes of each monomer separated
AqpZ_indexes = []

# And loop through the indexes
for i, index in enumerate(protein_indexes):
    
    # If the euclidean division of the position has no rest
    if i%474 == 0:
        # We want to append the sublist [i:i+474]
        AqpZ_indexes.append(protein_indexes[i:i+474])
        
# We have the indexes ! Now add them to our model class with an identifier
for i, index in enumerate(AqpZ_indexes):
    
    # The name
    name = "AqpZ_" + str(i+1)
    
    # And add the index
    model.add_indx(name, index)

    # Some space here if we want to add another index within each monomer
    # ///
    # ///
    # ///
    
    # And that's it
    
# And write a new .ndx now
model.write_ndx(output_index)



