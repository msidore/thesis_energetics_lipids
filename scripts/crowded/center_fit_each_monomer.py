#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, subprocess
import numpy
from argparse import ArgumentParser
# The folder where Friand Dur is located !
sys.path.append('/home/ub/bin/Friand_Dur')
from Model import Model

"""

Prepare a .sh file that will launch VMD 125 times and produce 125 volmaps, 
around each monomer.
Each treatment will be the wrapping around the monomer + fitting using fitframes

Usage: ./center_fit_each_monomer.py -f CBol_1of15_protlip_whole.xtc -c CBol_1of15_protlip_whole.pdb -n CBol_protlip_mono.ndx -o CBol

"""

######################## Parsing stuff ########################

parser = ArgumentParser(description="""  """)

# Named arguments
parser.add_argument("-f", "--file", help="The name of the trajectory input file")
parser.add_argument("-c", "--structure", help="The name of the structure (pdb) input file")
parser.add_argument("-o", "--output", help="The generic output name")
parser.add_argument("-n", "--ndx", help="The name of the index file /!\ MUST CONTAIN ALL MONOMERS")

args = parser.parse_args()

######################## Functions and miscellaneous ########################

# Input and output
trajfile = args.file
output_name = args.output
ndx_file = args.ndx
structure = args.structure

# VMD template - always get the same reference !
# And take the lipids around like 15 of the protein. Else each map is reaaaaally big.
VMD_template = """package require pbctools
mol load pdb %s xtc %s

proc fitframes { molid seltext } {
    set origin [atomselect top "index 0 to 473" frame 0]
    #~ set ref [atomselect $molid $seltext frame 0]
    set sel [atomselect $molid $seltext]
    set all [atomselect $molid all]
    
    set n [molinfo $molid get numframes]
    for { set i 1 } { $i < $n } { incr i } {
        $sel frame $i
        $all frame $i
        $all move [measure fit $sel $origin]
        }
    return
}

#~ set origin [atomselect top "index 0 to 473" frame 0]
#~ set sel [atomselect top "%s" frame 0]
#~ set all [atomselect top "all"]
#~ $all move [measure fit $sel $origin]

# Before wrapping, get our selection in a single resid
set mono [atomselect top "%s"]
$mono set resid 666666

# Wrap around the selection - and keep the first frame to fit on
pbc wrap -centersel "%s" -center com -first 1 -last last -compound resid

# Fit the selection
fitframes top "%s"

# Also write the trajectory, activate if debug
#~ set all2 [atomselect top "all"]
#~ animate write dcd %s.wrapp.dcd sel $all2 waitfor all
#~ $all2 writepdb %s.wrapp.pdb

# Delete the first frame ? It doesn't work. Keep the first, unwrapped frame for the volmap ...
#~ animate delete 0

# Volmaps - while discarding the first frame, which we can't do ...
volmap occupancy [atomselect top "(within 20 of %s) and (resname CDL2 CXL2)"] -res 1.0  -allframes -combine avg -o %s_CL.dx -checkpoint 0
volmap occupancy [atomselect top "(within 20 of %s) and (resname POPG DOPG DPPG)"] -res 1.0  -allframes -combine avg -o %s_PG.dx -checkpoint 0
volmap occupancy [atomselect top "(within 20 of %s) and (resname POPE DOPE DPPE DLPE)"] -res 1.0  -allframes -combine avg -o %s_PE.dx -checkpoint 0

quit
"""

volutil_template = """volutil -o %s -add %s %s
"""

# VMD command
VMD_command_template = "vmd_bin -dispdev text -e {tcl}\n"

######################## Main ########################

# Open the .sh script
out_ = open("get_volmaps.sh", "w")
out_.write("#!/bin/sh\n")

# Get the indexes
model = Model(ndx=ndx_file)
# Get 26: to get all the monomers
indexes = model.get_all_the_indexes()[26:]

# Keep track of the volmap names
vol_names = []

# Loop through all the monomers
for i, monomer in enumerate(indexes):
    
    # Get the minmax of this monomer, -1 because VMD starts at 0
    minmax = [min(monomer[1])-1, max(monomer[1])-1]
    
    # The selection is always the monomer
    sel = "index {:d} to {:d}".format(minmax[0], minmax[1])
    print "monomer {:d}, {:s}".format(i, sel)
    
    # The name of the volmap output
    volout = "{:s}_{:s}_occupancy".format(monomer[0], output_name)
    # Keep track of that name
    vol_names.append(volout)
    
    # Write the temporary tcl script
    temp_tcl_name = "temp_tcl_"+str(i)
    temp_tcl = open(temp_tcl_name, "w")
    # Many stuff is because debug in this line
    temp_tcl.write(VMD_template % (structure, trajfile, sel, sel, sel, sel, volout, \
            volout, sel, volout, sel, volout, sel, volout))
    temp_tcl.close()

    # Add the call to this script in the .sh
    out_.write(VMD_command_template.format(tcl=temp_tcl_name))
    
    # And add the rm
    out_.write("rm {tcl}\n".format(tcl=temp_tcl_name))

# Close the .sh script
out_.close()

# And another script to average the volmaps
out_ = open("average_volmaps.sh", "w")
out_.write("#!/bin/sh\n")

# And the corresponding tcl script, with the header
tcl_ = open("temp_volutil", "w")
tcl_.write("package require volutil\n")

# In that tcl script, we want all the maps, which are
CL_vol = [x + "_CL.dx" for x in vol_names]
PG_vol = [x + "_PG.dx" for x in vol_names]
PE_vol = [x + "_PE.dx" for x in vol_names]

# And there are len(CL_vol) maps
num_maps = len(CL_vol)

# Also, volutil only lets us average two maps at once, so we need to make something 
# a bit dirty to average the 125 maps ...
# Take a temporary intermediate name
previous_name_CL = CL_vol[0]
previous_name_PG = PG_vol[0]
previous_name_PE = PE_vol[0]

# That means loop
# Starts at 1 and doesn't end at num_maps+1 because we're doing the +1 index thing
for i in range(1, num_maps):

    # Get a name for the intermediate maps
    intermediate_name_CL = "{:s}_volmap_intermediate_CL_{:d}_{:d}.dx".format(output_name, i, i+1)
    intermediate_name_PG = "{:s}_volmap_intermediate_PG_{:d}_{:d}.dx".format(output_name, i, i+1)
    intermediate_name_PE = "{:s}_volmap_intermediate_PE_{:d}_{:d}.dx".format(output_name, i, i+1)
    
    # And add the volutil commands
    tcl_.write(volutil_template % (intermediate_name_CL, previous_name_CL, CL_vol[i]))
    tcl_.write(volutil_template % (intermediate_name_PG, previous_name_PG, PG_vol[i]))
    tcl_.write(volutil_template % (intermediate_name_PE, previous_name_PE, PE_vol[i]))
    
    # And erase the previous_name with this one
    previous_name_CL = intermediate_name_CL
    previous_name_PG = intermediate_name_PG
    previous_name_PE = intermediate_name_PE
    
# Add a quit
tcl_.write("quit")

# And just add the call to this script in the .sh
out_.write(VMD_command_template.format(tcl="temp_volutil"))

# Close both scripts
tcl_.close()
out_.close()









