#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
# The folder where Friand Dur is located !
sys.path.append('/home/ub/bin/Friand_Dur')
from Model import Model

""" 

Takes a .ndx and creates a .sh with gmx rdf calls using all possible combinations in that .ndx

"""

######################## Functions and miscellaneous ########################

# The .ndx file which contains only the indexes we want to use in the RDF analysis
ndx_file = "edges.ndx"

# Some files used in rdf - trajectory, topology, etc.
# Think of using a file that contains only proteins, for obvious reasons (speed ?)
trajectory = "/media/ub/DATA/Trajectories/CBol/CBol_19500000to20000000ps_prots.xtc"
topology = "/media/ub/DATA/Trajectories/CBol/CBol_prots.tpr"

# A sample line for rdf calls
# Basically, feed 2 numbers to gmx rdf, the selections
# -xy calculates the RDF only on the xy plane, which is what should be for
# membrane proteins
sample_rdf = """cat << EOF | gmx rdf -f {:s} -s {:s} -n {:s} -o {:s} -cn {:s} -xy
{:d}
{:d}
EOF

"""

######################## Main ########################

# The output .sh file, with its header
out_ = open("all_the_rdf.sh", "w")
out_.write("#!/bin/sh\n")

# Get all the indexes
# Get the indexes
model = Model(ndx=ndx_file)
indexes = model.get_all_the_indexes()

# Each index is (name, [indexes]), so
# For each index couple (without repetition)
for i1, index1 in enumerate(indexes):
    for i2, index2 in enumerate(indexes):
        
        # Avoid repetitions
        # This is not in the loop (indexes[i1:] for instance) since
        # i2 would start too early instead of at the right index
        if i2 < i1: continue
        
        # The index names, while replacing any weird stuff like &
        i1_name = index1[0].replace("&", "and")
        i2_name = index2[0].replace("&", "and")
        
        # The output name will be
        xvg_out = "{:s}_{:s}.xvg".format(i1_name, i2_name)
        # Also, get the cumulative rdf for verification purpose
        xvg_cumsum = "{:s}_{:s}_cumsum.xvg".format(i1_name, i2_name)
        
        # Luckily, i1 and i2 are the index numbers used in gromacs
        rdf_command = sample_rdf.format(trajectory, topology, ndx_file, xvg_out, xvg_cumsum, i1, i2)
        
        # And write the command
        out_.write(rdf_command)

# Don't forget to close the file
out_.close()
