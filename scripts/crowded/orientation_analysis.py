#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, subprocess
import numpy
from argparse import ArgumentParser
import matplotlib
matplotlib.use('PS')
import matplotlib.pyplot as plt
import pylab
from textwrap import wrap
# The folder where Friand Dur is located !
sys.path.append('/home/ub/bin/Friand_Dur')
from Model import Model
# Aaand the spinpack
import spinpack
# And ofc MDAnalysis
import MDAnalysis

"""

Designed to do the analysis of the relative orientations of all the monomers
in my big crowded membrane.

Just ./orientation_analysis.py

And convert all the .ps into .png with something like this:
***incoming***

"""

######################## Parsing stuff ########################

# No parser, there are too many arguments, just check them in this script

######################## Functions and miscellaneous ########################

# Put everything here, testing is more important right now
# The trajectory
trajectory = "CBol_1of15_prot_whole.xtc"
# The GRO file that will serve as a topology
topol = "npt3_prot_whole.gro"
# The .ndx file
ndx_file = "CBol_monomers.ndx"
# This should be our reference file for the spin
spinref = "/media/ub/DATA/Miscellaneous/spinDimer_reference.pdb"
# A generic output name
outname = "oriis"

# Some selections - keep in mind each monomer is 474 particles long
# And MDAnalysis starts at 1 (so it's 1:474 or 475:948 for example)
spin_namd = "name BAS and bynum 475:948"
spin_gro = "name BB and bynum {:d}:{:d}"
# The first 3 selections are in the first monomer with 55, 364 and 370
# The last one is on the second monomer with 106
selection_dihed1 = "(bynum {:d}:{:d} and name BAS and around 7.0 (bynum {:d})) or bynum {:d}"
selection_dihed2 = "(bynum {:d}:{:d} and name BAS and around 7.0 (bynum {:d})) or bynum {:d}"
selection_dihed3 = "(bynum {:d}:{:d} and name BAS and around 7.0 (bynum {:d})) or bynum {:d}"
selection_dihed4 = "(bynum {:d}:{:d} and name BAS and around 7.0 (bynum {:d})) or bynum {:d}"
# And the selection for the distance
# Yes, I know it's the same as spin_gro, it's for clarity
sel_dist = "name BB and bynum {:d}:{:d}"

# The directory to put all the pictures
end_directory = "orient/"

######################## Functions and miscellaneous ########################

def dihedral(p):
    """ Dihedral angle between 4 (x, y, z) points in the list p """
    p = numpy.array(p)
    b = p[:-1] - p[1:]
    b[0] *= -1
    v = numpy.array( [ v - (v.dot(b[1])/b[1].dot(b[1])) * b[1] for v in [b[0], b[2]] ] )
    # Normalize vectors
    v /= numpy.sqrt(numpy.einsum('...i,...i', v, v)).reshape(-1,1)
    b1 = b[1] / numpy.linalg.norm(b[1])
    x = numpy.dot(v[0], v[1])
    m = numpy.cross(v[0], b1)
    y = numpy.dot(m, v[1])
    return numpy.degrees(numpy.arctan2( y, x ))

def shift_center(conformation):
    """Center and typecheck the conformation"""

    conformation = numpy.asarray(conformation)
    if not conformation.ndim == 2:
        raise ValueError('conformation must be two dimensional')
    _, three = conformation.shape
    if not three == 3:
        raise ValueError('conformation second dimension must be 3')

    centroid = numpy.mean(conformation, axis=0)
    centered = conformation - centroid
    return centered

######################## Main ########################

# The general idea:
# - Charge the .ndx, to get the indexes of each monomer
# - Charge the trajectory in MDAnalysis
# - Also charge the .gro as reference and get the coordinates of each atom
# - Make a big loop over each frame
# -- Get spin and dihedral for each pair using spinpack.spin
# -- Put that into a heatmap

# Get the indexes
model = Model(ndx=ndx_file)
# 2: because the first two aren't the monomers
indexes = model.get_all_the_indexes()[2:]

# Charge the trajectory
mobile = MDAnalysis.Universe(topol, trajectory)

# Charge the reference
reference = MDAnalysis.Universe(spinref)
# Get the coordinates of the atoms of the backbone
refer_atoms = reference.select_atoms(spin_namd).positions

# Loop through the trajectory
for frame in mobile.trajectory:
    
    # The frame
    ts = frame.frame
    
    # Debug
    if ts < 500:
        continue
    
    # Two lists which will have the spins and dihedrals
    spins, dihedrals, distances = [], [], []
    
    # Loop through each monomer
    for i1 in range(len(indexes)):
        for i2 in range(i1+1, len(indexes)):
            
            # The coordinates of the second monomer are accessed with the
            # Indexes - which are the second position
            # That means get the minmax and feed it into MDAnalysis' selections
            # First, the indexes of the first monomer are
            mono1_minmax_index = [min(indexes[i1][1]), max(indexes[i1][1])]
            # And for the second
            mono2_minmax_index = [min(indexes[i2][1]), max(indexes[i2][1])]
            
            ####### Firstly in fact, the distances #######
            
            # Get the center of mass of each monomer
            com1 = mobile.select_atoms(sel_dist.format(mono1_minmax_index[0], \
                    mono1_minmax_index[1])).center_of_mass()
            com2 = mobile.select_atoms(sel_dist.format(mono2_minmax_index[0], \
                    mono2_minmax_index[1])).center_of_mass()
            
            # And the vector joining the two is
            vector = com2 - com1
            
            # And its norm is
            dist = numpy.linalg.norm(vector)
            
            # Now, apply a cutoff ! If the distance is > than 65, throw it
            if dist > 65: continue
            
            # And append it if it's ok
            distances.append(dist)
            
            ####### Then, the spin angle #######

            # Get the atoms
            mono2_spin = mobile.select_atoms(spin_gro.format(mono2_minmax_index[0], \
                    mono2_minmax_index[1])).positions
                    
            # debug
            #~ print spin_gro.format(mono2_minmax_index[0], \
                    #~ mono2_minmax_index[1])
                    
            # Now we have mono2_spin to compare to refer_atoms
            # First, center both and transform as contiguous array, to throw it
            # In the C function
            coords_center = numpy.ascontiguousarray(shift_center(mono2_spin), dtype=numpy.float64)
            reference_center = numpy.ascontiguousarray(shift_center(refer_atoms), dtype=numpy.float64)
            
            # Get the spinangle
            spinangle = spinpack.spin.get_spinangle_traj(coords_center, reference_center, len(reference_center))
            
            # Append it
            spins.append(spinangle)
            
            ####### And lastly, the dihedrals #######
    
            # Get the 4 coordinates of the centers of mass for the dihedral
            
            # First one is 55 on monomer 1
            coord_d1 = mobile.select_atoms(selection_dihed1.format(mono1_minmax_index[0], \
                    mono1_minmax_index[1], mono1_minmax_index[0] + 55, \
                    mono1_minmax_index[0] + 55)).center_of_mass()
                    
            # Second is 364 on monomer 1
            coord_d2 = mobile.select_atoms(selection_dihed1.format(mono1_minmax_index[0], \
                    mono1_minmax_index[1], mono1_minmax_index[0] + 364, \
                    mono1_minmax_index[0] + 364)).center_of_mass()
    
            # Third is 370 on monomer 1
            coord_d3 = mobile.select_atoms(selection_dihed1.format(mono1_minmax_index[0], \
                    mono1_minmax_index[1], mono1_minmax_index[0] + 370, \
                    mono1_minmax_index[0] + 370)).center_of_mass()
    
            # And fourth is 106 on monomer 2
            coord_d4 = mobile.select_atoms(selection_dihed1.format(mono2_minmax_index[0], \
                    mono2_minmax_index[1], mono2_minmax_index[0] + 106, \
                    mono2_minmax_index[0] + 106)).center_of_mass()
                    
            # The dihedrals can therefore be computed
            dihed = dihedral([coord_d1, coord_d2, coord_d3, coord_d4])
            
            # And appended
            dihedrals.append(dihed)
            
    # Ok, we have all the spins, dihedrals and distances
    # Put it into a heatmap
    # Heatmap matplotlib
    plt.figure('heatmap' + str(ts))
    plt.xlabel('Dihedral angle (degree)')
    plt.ylabel('Spin angle (degree)')
    plt.axis([-180, 180, -180, 180])
    plt.scatter(dihedrals, spins, c=distances, edgecolors='none', s=[2 for x in distances])
    # And for gray points
    #~ plt.scatter(dihedrals, spins, c=['0' for x in distances], edgecolors='none', s=[2 for x in steps])
    cb = plt.colorbar()
    cb.set_label(r'Distance ($\AA$)')
    # Comment and uncomment if you want only to see the result
    plt.savefig(end_directory + outname + str(ts) + ".ps")
    plt.close()
    
    # Debug
    #~ plt.figure('debug' + str(ts))
    #~ plt.xlabel('Pair')
    #~ plt.ylabel('Spin angle (degree)')
    #~ pairs = len(spins)
    #~ plt.axis([0, pairs, -180, 180])
    #~ plt.plot(range(pairs), spins, 'ro')
    #~ # Comment and uncomment if you want only to see the result
    #~ plt.savefig(end_directory + outname + str(ts) + "debug.ps")
    #~ plt.close()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

























