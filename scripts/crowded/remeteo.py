#!/usr/bin/python
# -*- coding: utf-8 -*-

# Ignore any DeprecationWarning since one MDAnalysis module isn't up to date
# ~ import warnings
# ~ warnings.filterwarnings("ignore", category=DeprecationWarning)
# ~ warnings.filterwarnings("ignore", category=FutureWarning)

import string, os
import matplotlib
import MDAnalysis
# ~ import MDAnalysis.visualization.streamlines
##########################################
# DEBUG
import debug_streamlines
##########################################
import matplotlib.pyplot, sys,numpy
import cPickle as pickle
from argparse import ArgumentParser

######################## Parsing stuff ########################

parser = ArgumentParser(description="""Meteo maaaaan, meteo guuuuuy, here's a lipid anticyclone, here's another on the other leaflet !""")
parser.add_argument("-f", "--first", help="First frame of the couple", required=True)
parser.add_argument("-l", "--last", help="Last frame of the couple", required=True)
args = parser.parse_args()
startf = int(args.first)
endf = int(args.last)

######################## Functions and miscellaneous ########################

input_prot_gro = "/media/ub/DATA/Trajectories/CBol/old/prots.gro"
input_prot_xtc = "/media/ub/DATA/Trajectories/CBol/meteo/prots.xtc"      # xtc file for the proteins
input_leaflet1_gro = "/media/ub/DATA/Trajectories/CBol/meteo/initial_0.gro"
input_leaflet1_xtc = "/media/ub/DATA/Trajectories/CBol/meteo/leaf0.xtc"  # xtc file for the first leaflet
input_leaflet2_gro = "/media/ub/DATA/Trajectories/CBol/meteo/initial_1.gro"
input_leaflet2_xtc = "/media/ub/DATA/Trajectories/CBol/meteo/leaf1.xtc"  # xtc file for the second leaflet
input_selection = "name PO4 or name PO41"                              # selection for the MDAnalysis by default  'name PO4' (see https://mdanalysis.googlecode.com/git/package/doc/html/documentation_pages/selections.html )
grid = 20                                                              # size of the grid - ie resolution
xmin_in = 0                                                            # xmin: in Angstrom
xmax_in = 520                                                          # xmax: in Angstrom
ymin_in = 0                                                            # ymin: in Angstrom
ymax_in = 520                                                          # ymax: in Angstrom
dt = 1                                                                 # choose the time step, in general 1
filename = "streamline"                                                # output filename for the picture
extension = ".png"                                                     # extension of the picture (.png, .svg, etc ..), need to be managed by Matplotlib

couple = (startf, endf)
print "The frames {:d} and {:d} will be treated as continuous here".format(startf, endf)

print "Loading the protein trajectory ..."
U = MDAnalysis.Universe(input_prot_gro,input_prot_xtc)
protein = U.select_atoms('all')

print "Loading the first leaflet trajectory ..."
U2 = MDAnalysis.Universe(input_leaflet1_gro,input_leaflet1_xtc)
PO4_1 = U2.select_atoms('all')
PO4_1 = U2.select_atoms(input_selection)

print "Loading the second leaflet trajectory ..."
U3 = MDAnalysis.Universe(input_leaflet2_gro,input_leaflet2_xtc)
PO4_2 = U3.select_atoms('all')
PO4_2 = U3.select_atoms(input_selection)

print "All three trajectories loaded"

nb_squares_x = (xmax_in-xmin_in)/grid - 1
nb_squares_y = (ymax_in-ymin_in)/grid - 1

print "Number of cells in x: {:d}".format(nb_squares_x)
print "Number of cells in y: {:d}".format(nb_squares_y)

# Calculate displacement following x (u1) for each vector, displacement following y (v1) for each vector, average of the displacementand standard deviation of the displacement for U2. More info, http://mdanalysis.googlecode.com/git-history/develop/package/doc/html/documentation_pages/visualization/streamlines.html
# The input .gro file is just there as a structure for the Universe object
# ~ u1, v1, average_displacement1,standard_deviation_of_displacement1 = MDAnalysis.visualization.streamlines.generate_streamlines(input_leaflet1_gro, input_leaflet1_xtc, grid_spacing=grid, MDA_selection=input_selection, start_frame=startf, end_frame=endf, xmin=xmin_in, xmax=xmax_in, ymin=ymin_in , ymax=ymax_in, maximum_delta_magnitude=5.0, num_cores=1)
u1, v1, average_displacement1,standard_deviation_of_displacement1 = debug_streamlines.generate_streamlines_single(input_leaflet1_gro, input_leaflet1_xtc, grid_spacing=grid, MDA_selection=input_selection, start_frame=startf, end_frame=endf, xmin=xmin_in, xmax=xmax_in, ymin=ymin_in , ymax=ymax_in, maximum_delta_magnitude=5.0, num_cores=1)

# ~ print u1
# ~ print v1
# ~ print average_displacement1
# ~ print standard_deviation_of_displacement1

################# NOTE ################# 
# I removed the multiprocessing part which imo was the problem
# Indeed, it seems to work now
################# NOTE ################# 

# Calculate displacement following x (u2) for each vector, displacement following y (v2) for each vector, average of the displacementand standard deviation of the displacement for U3. More info, http://mdanalysis.googlecode.com/git-history/develop/package/doc/html/documentation_pages/visualization/streamlines.html
u2, v2, average_displacement2,standard_deviation_of_displacement2 = debug_streamlines.generate_streamlines_single(input_leaflet2_gro, input_leaflet2_xtc, grid_spacing=grid, MDA_selection=input_selection, start_frame=startf, end_frame=endf, xmin=xmin_in, xmax=xmax_in, ymin=ymin_in , ymax=ymax_in, maximum_delta_magnitude=5.0, num_cores=1)

# Retrieve the data in the trajectory and store them in an intermediate file called streamline_test.p

# Store the data from protein trajectory
for ts in U.trajectory:
    if ts.frame == startf:
        protein_coords = protein.positions
        pickle.dump([u1,v1,u2,v2,average_displacement1,standard_deviation_of_displacement1,average_displacement2,standard_deviation_of_displacement2, protein_coords],open('junk/streamline_store.p'+str(startf),'wb'))
        print "Stored protein coordinates at frame {:d}".format(ts.frame)
        break
    else:
        pickle.dump([u1,v1,u2,v2,average_displacement1,standard_deviation_of_displacement1,average_displacement2,standard_deviation_of_displacement2],open('junk/streamline_store.p'+str(startf),'wb'))

# End of the retrieving part

# Return evenly spaced numbers over a specified interval for x and y
x = numpy.linspace(xmin_in,xmax_in,nb_squares_x)
y = numpy.linspace(ymin_in,ymax_in,nb_squares_y)

# Read the data stored in the file streamline_test.p
pickled_data = pickle.load(open('junk/streamline_store.p'+str(startf),'rb'))

# Reconstitute coordinates (with the protein coordinates added)
u1,v1,u2,v2,average_displacement1,standard_deviation_of_displacement1,average_displacement2,standard_deviation_of_displacement2, protein_coords = pickled_data

# Print a few informations about the average displacement and standard deviation
print "------"
print "avg_disp1: "+str(average_displacement1)
print "std_dev1: "+str(standard_deviation_of_displacement1)
print "------"
print "avg_disp2: "+str(average_displacement2)
print "std_dev2: "+str(standard_deviation_of_displacement2)
print "------"

# ~ print u1
# ~ print v1

# Calculate the speed for each vector
speed1 = numpy.sqrt(u1*u1 + v1*v1)
speed2 = numpy.sqrt(u2*u2 + v2*v2)
# ~ print speed1
# ~ print speed2
# ~ print x
# ~ print y
# ~ print 3.0*speed2/speed2.max()

# Define matplotlib figure and axes
fig = matplotlib.pyplot.figure(startf)

# Use the matplotlib streamplot (more info here: http://matplotlib.org/examples/images_contours_and_fields/streamplot_demo_features.html)
# ie the density can be changed: increasing the number will result in increasing the number of streamlines

# The big graph --------------
# Define position of the graph and create a squarred graph
gs1 = matplotlib.gridspec.GridSpec(2, 2)
gs1.update(left=0.1, right=0.58, wspace=0.02)
ax = matplotlib.pyplot.subplot(gs1[:,:], aspect='equal')
# Calculate the streamlines using matplotlib streamplot
s1= ax.streamplot(x,y,u2,v2,density=(8,8),color=speed2,linewidth=3.0*speed2/speed2.max(),cmap=matplotlib.pyplot.cm.PuBu)
s2= ax.streamplot(x,y,u1,v1,density=(8,8),color=speed1,linewidth=3.0*speed1/speed1.max(),cmap=matplotlib.pyplot.cm.OrRd)
# Set the transparency of the 2nd plot to better see the one in background
s2.lines.set_alpha(0.8)
# Define the labels of the graph
ax.set_ylabel('X [A]', size=15)
ax.set_xlabel('Y [A]', size=15)
# Define the boundaries of the graph
ax.set_xlim([xmin_in,xmax_in])
ax.set_ylim([ymin_in,ymax_in])
ax.set_xticks(numpy.arange(xmin_in,xmax_in,200))
ax.set_yticks(numpy.arange(ymin_in,ymax_in,200))
# End big graph --------------

# The 2 small graphs ----------------------------
# Define position of the graph and create a squarred graph
gs2 = matplotlib.gridspec.GridSpec(4, 2)
gs2.update(left=0.63, right=0.95, hspace=1.2,wspace=0.1)

# Small graph 1
ax1 = matplotlib.pyplot.subplot(gs2[:2, :],aspect='equal')
# Calculate the streamlines using matplotlib streamplot
im1 = ax1.streamplot(x,y,u1,v1,density=(8,8),color=speed1,linewidth=3.0*speed1/speed1.max(),cmap=matplotlib.pyplot.cm.OrRd)
# Define the colorbar for graph1
cbar1 = fig.colorbar(im1.lines,ax=ax1,orientation='vertical',format="%.1f")
cbar1.set_label('Displacement [A] leaflet1',size=10)
# Define the labels of the graph
ax1.tick_params(axis='both', labelsize=10)
# Define the boundaries of the graph
ax1.set_xlim([xmin_in,xmax_in])
ax1.set_ylim([ymin_in,ymax_in])
ax1.set_xticks(numpy.arange(xmin_in,xmax_in,200))
ax1.set_yticks(numpy.arange(ymin_in,ymax_in,200))

# Small graph 2
ax2 = matplotlib.pyplot.subplot(gs2[-2:, :],aspect='equal')
# Calculate the streamlines using matplotlib streamplot
im2 = ax2.streamplot(x,y,u2,v2,density=(8,8),color=speed2,linewidth=3.0*speed2/speed2.max(),cmap=matplotlib.pyplot.cm.PuBu)
# Define the colorbar for graph2
cbar2 = fig.colorbar(im2.lines,ax=ax2,orientation='vertical',format="%.1f")
cbar2.set_label('Displacement [A] leaflet2',size=10)
# Define the labels of the graph
ax2.tick_params(axis='both', labelsize=10)
# Define the boundaries of the graph
ax2.set_xlim([xmin_in,xmax_in])
ax2.set_ylim([ymin_in,ymax_in])
ax2.set_xticks(numpy.arange(xmin_in,xmax_in,200))
ax2.set_yticks(numpy.arange(ymin_in,ymax_in,200))
# End of the 2 small graphs --------------------------


# Plot the protein coordinates with small purple circles
prot_x = protein_coords[...,0]
prot_y = protein_coords[...,1]
ax.plot(prot_x,prot_y,'o',markerfacecolor='#660066',markeredgecolor='#660066',markersize=0.8,alpha=0.5)
ax1.plot(prot_x,prot_y,'o',markerfacecolor='#660066',markeredgecolor='#660066',markersize=0.8,alpha=0.5)
ax2.plot(prot_x,prot_y,'o',markerfacecolor='#660066',markeredgecolor='#660066',markersize=0.8,alpha=0.5)

# Definition of the figure name:
aligned_i = "_{:010d}".format(startf)
figure_name = filename+aligned_i+extension
fig.savefig("results/"+figure_name,dpi=200)

print "{:s} saved".format(filename+aligned_i+extension)

# Cleanup !
os.remove("junk/streamline_store.p"+str(startf))
print "{:s} deleted".format("junk/streamline_store.p"+str(startf))
































