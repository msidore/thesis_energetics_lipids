proc countnum { nbframe Atomsel} {
$Atomsel frame $nbframe
$Atomsel update
return [$Atomsel num]
}

proc countsel { molid outfile POPG20 DOPG20 DPPG20 POPE20 DOPE20 DLPE20 DPPE20 CXL220 CDL220} {
  set outfile [open $outfile w]
  set n [molinfo $molid get numframes]
  puts $outfile "TS POPG DOPG DPPG POPE DOPE DLPE DPPE CXL2 CDL2"
  for { set i 0 } { $i < $n } { incr i } {
  set POPG [countnum $i $POPG20]
  set DOPG [countnum $i $DOPG20]
  set DPPG [countnum $i $DPPG20]
  set POPE [countnum $i $POPE20]
  set DOPE [countnum $i $DOPE20]
  set DLPE [countnum $i $DLPE20]
  set DPPE [countnum $i $DPPE20]
  set CXL2 [countnum $i $CXL220]
  set CDL2 [countnum $i $CDL220]
  puts $outfile "$i $POPG $DOPG $DPPG $POPE $DOPE $DLPE $DPPE $CXL2 $CDL2"
  }
  close $outfile
  return
}

mol new {/media/ubuntu/DATA/Trajectories/vicl/vicl_start.gro} type {gro} first 0 last -1 step 1 waitfor 1
mol addfile {/media/ubuntu/DATA/Trajectories/vicl/vicl_wrapped_stable_1of3_third.xtc} type {xtc} first 0 last -1 step 1 waitfor -1 0

set POPG20 [atomselect 0 "name PO4 and (same resid as (resname POPG and within 21.2 of name BB))"]
set DOPG20 [atomselect 0 "name PO4 and (same resid as (resname DOPG and within 21.2 of name BB))"]
set DPPG20 [atomselect 0 "name PO4 and (same resid as (resname DPPG and within 21.2 of name BB))"]
set POPE20 [atomselect 0 "name PO4 and (same resid as (resname POPE and within 21.2 of name BB))"]
set DOPE20 [atomselect 0 "name PO4 and (same resid as (resname DOPE and within 21.2 of name BB))"]
set DLPE20 [atomselect 0 "name PO4 and (same resid as (resname DLPE and within 21.2 of name BB))"]
set DPPE20 [atomselect 0 "name PO4 and (same resid as (resname DPPE and within 21.2 of name BB))"]
set CXL220 [atomselect 0 "name PO41 and (same resid as (resname CXL2 and within 21.2 of name BB))"]
set CDL220 [atomselect 0 "name PO41 and (same resid as (resname CDL2 and within 21.2 of name BB))"]

countsel 0 vicltab3.csv $POPG20 $DOPG20 $DPPG20 $POPE20 $DOPE20 $DLPE20 $DPPE20 $CXL220 $CDL220
exit
