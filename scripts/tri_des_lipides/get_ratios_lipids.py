#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, subprocess
import MDAnalysis
import numpy
from argparse import ArgumentParser
import matplotlib
matplotlib.use('PS')
import matplotlib.pyplot as plt
import pylab
from textwrap import wrap

"""
Transforms a table with numbers of lipids throughout a trajectory into timeseries of % and overall %

Usage:
get_ratios_lipids.py -f vicltab.csv -o vicl -s vicl_start.gro -g
"""

######################## Parsing stuff ########################

directory = ""
graphs_switch = False

parser = ArgumentParser(description="""  """)

# Named arguments
parser.add_argument("-f", "--file", help="The name of the input xtc file")
parser.add_argument("-s", "--structure", help="The name of the input topology file - usually a .gro, not a .top")
parser.add_argument("-o", "--output", help="The generic output name")

# Optional arguments
parser.add_argument("-d", "--directory", help="Output directory ? Default is the current directory.")
parser.add_argument("-g", "--graphs", help="Make a bunch of graphs !", action="store_true")

args = parser.parse_args()

######################## Directory stuff ########################

if args.directory:
    # Checks if the directory has a /
    if args.directory[-1] == "/":
        directory += args.directory
    else:
        directory += args.directory + "/"
    subprocess.Popen("mkdir " + directory, shell=True, stderr=subprocess.PIPE).wait()

######################## Functions and miscellaneous ########################

csv = args.file
gro = args.structure
output_name = args.output
if args.graphs:
    graphs_switch = True

nomnom = 3

######################## Main ########################

if __name__ == '__main__':

    # Start by opening an output file which will have a few datas
    out = open(output_name + "_endtable.csv", "w")

    # First, check what kinds of lipids we have inside that box, in the reference structure
    reference = MDAnalysis.Universe(gro)

    # Selection to select all lipids
    lipids = reference.select_atoms("(not name BB and not name SC1 and not name SC2 and not name SC3 and not name SC4 and not name W and not resname ION)")

    # Fill a set with the resnames to have a list of lipids
    lip_list = set(lipids.resnames)
    lip_list = list(lip_list)

    # Do a dictionary of those; that will be useful
    all_lipids_num = {}
    all_lipids_per = {}

    # First part of the output
    out.write("Whole membrane\n")
    out.write("Category;" + ";".join([lip_list[x] for x in range(len(lip_list))]) + ";Total\nLipid count;")

    # Print in the outfile the total numbers of lipids ...
    line = ""
    for lip in lip_list:
        lipid = reference.select_atoms("(not name BB and not name SC1 and not name SC2 and not name SC3 and not name SC4 and not name W and not resname ION) and (resname " + lip + ")")
        number = len(set(lipid.resids))
        line += str(number) + ";"
        all_lipids_num[lip] = number
    out.write(line)
    # Total
    out.write(str(round(numpy.sum([float(x) for x in line.split(";")[:-1]]), nomnom)))
    out.write("\nLipid fractions;")
    #~ out.write("Total lipid numbers: " + line + "\n")
    # And their fractions !
    numbers = []
    for lip in lip_list:
        lipid = reference.select_atoms("(not name BB and not name SC1 and not name SC2 and not name SC3 and not name SC4 and not name W and not resname ION) and (resname " + lip + ")")
        number = len(set(lipid.resids))
        numbers.append(round(float(number), nomnom))
    total = numpy.sum(numbers)
    for i in range(len(lip_list)):
        all_lipids_per[lip_list[i]] = numbers[i]*100/total
    percents = [str(round(x/total, nomnom)) for x in numbers]
    out.write(";".join([percents[x] for x in range(len(lip_list))]) + ";" + str(round(numpy.sum([float(x) for x in percents]),nomnom)) + "\n\n")

    # Fine from this point
    # Now second part of the file
    out.write("Around 21.2 of the protein\n")
    out.write("Category;" + ";".join([lip_list[x] for x in range(len(lip_list))]) + ";Total;Formula\nLipid count means;")

    # First open the csv and get lines
    csv_open = open(csv, "r").readlines()

    # Header is the first line
    header = csv_open[0].split()
    body = csv_open[1:]

    # Reorganize the body to have n timeseries corresponding with the n words the header have
    # And before that, create a "total" timeserie, that contain the total number of lipids per line
    body = [[float(j) for j in x.split()] for x in body]
    total = [numpy.sum(body[i][1:]) for i in range(len(body))]
    body = [[body[i][k] for i in range(len(body))] for k in range(len(header))]
    timesteps = body[0]

    # Get a few numbers from the total serie
    total_mean = numpy.mean(total)
    total_stdev = numpy.std(total)

    # Print those data in the output
    #~ out.write("Totals: mean %f stdev %f -- \n\n" % (total_mean, total_stdev))

    # Print a header line in the output
    #~ out.write(" ".join([x + "mean " + x + "stdev " + x + "mean\% " for x in header[1:]]) + "\n")

    # We have all the timeseries now ! (How I love such easy 1 line text manipulation)
    # Loop through these while getting the sum, stdev, mean, % and make graphs
    # And a small variable, because graphs
    file_list = ""

    # A dict for the "around" part
    around_lips = {}
    for i in range(1, len(body)):

        # Get the mean of the number of lipids and its stdev
        mean = numpy.mean(body[i])
        stdev = numpy.std(body[i])

        # Transform those numbers of lipids into % and get the mean and stdev in % - to be used in graphs
        percents = [body[i][j]*100/total[j] for j in range(len(total))]
        #~ percents_mean = numpy.mean(percents)
        #~ percents_stdev = numpy.std(percents)

        # Only divide by the total mean, not mean and stdev for each frame
        percents_mean = mean/total_mean

        # Put them in the dict
        around_lips[header[i]] = [mean, stdev, percents_mean]

        # Print these in the output
        #~ out.write("%f %f %f " % (mean, stdev, percents_mean))

        # Make a bunch of graphs !
        if graphs_switch == True:

            # 20fs per step, one frame every 10000 steps and only one of 3 frames stored for this
            # Let's transform the timesteps variable (containing all the timestep numbers) into microseconds
            microseconds = [(x*20.0*30000.0)/1000000000.0 for x in timesteps]

            #~ percmean = numpy.mean(percents)
            percmean = percents_mean*100
            percallmean = all_lipids_per[header[i]]

            # Make the graphs, lipid numbers and %, of that timeserie !
            plt.figure(output_name + str(i))
            plt.xlabel(r'Time ($\mu$s)', fontsize=18)
            plt.ylabel('Lipid number', fontsize=18)
            plt.title("\n".join(wrap("Lipid numbers for " + header[i])))
            plt.axis([0, max(microseconds), min(body[i]) -3, max(body[i]) +3])
            plt.tick_params(labelsize=18)
            plt.plot(microseconds, body[i], label="Lipid number timeserie")
            plt.plot(microseconds, [mean for x in microseconds], 'g', label="Local lipid number mean")
            plt.legend()
            plt.savefig(output_name + "_lip" + header[i] + ".ps")
            file_list += output_name + "_lip" + header[i] + ".ps "

            plt.figure(output_name + "_" + str(i))
            plt.xlabel(r'Time ($\mu$s)', fontsize=18)
            plt.ylabel('Lipid percents', fontsize=18)
            plt.title("\n".join(wrap("Lipid percents for " + header[i])))
            plt.axis([0, max(microseconds), min(percents) -3, max(percents) +3])
            plt.tick_params(labelsize=18)
            l1 = plt.plot(microseconds, percents, label="Percent timeserie")
            l2 = plt.plot(microseconds, [percmean for x in microseconds], 'g', label="Local percent mean")
            l3 = plt.plot(microseconds, [percallmean for x in range(len(microseconds))], 'r', label="Overall percent mean")
            plt.legend()
            plt.savefig(output_name + "_per" + header[i] + ".ps")
            file_list += output_name + "_per" + header[i] + ".ps "

    # We have all the graphs in file_list
    bash_command = "cat " + file_list + "> " + output_name + "_all.ps"
    subprocess.Popen(bash_command, shell=True, stderr=subprocess.PIPE).wait()
    bash_command = "epstopdf " + "--exact " + output_name + "_all.ps"
    subprocess.Popen(bash_command, shell=True, stderr=subprocess.PIPE).wait()

    # Now write things - the count first
    for i in range(len(body)-1):
        out.write(str(round(around_lips[lip_list[i]][0], nomnom)) + ";")
    out.write(str(round(numpy.sum([around_lips[x][0] for x in lip_list]), nomnom)) + "\nLipid count stdev;")
    # The stdev
    for i in range(len(body)-1):
        out.write(str(round(around_lips[lip_list[i]][1], nomnom)) + ";")
    out.write(str(round(total_stdev, nomnom)) + "\nLipid fractions;")
    # The fraction
    for i in range(len(body)-1):
        out.write(str(round(around_lips[lip_list[i]][2], nomnom)) + ";")
    out.write(str(round(numpy.sum([around_lips[x][2] for x in lip_list]), nomnom)) + "\nFluctuations;")
    fluctuations = []
    for i in range(len(body)-1):
        out.write(str(round(around_lips[lip_list[i]][2]-(around_lips[lip_list[i]][0]-around_lips[lip_list[i]][1])/numpy.sum([around_lips[x][0] for x in lip_list]), nomnom)) + ";")
        fluctuations.append(around_lips[lip_list[i]][2]-(around_lips[lip_list[i]][0]-around_lips[lip_list[i]][1])/numpy.sum([around_lips[x][0] for x in lip_list]))
    out.write(";(Fraction-(Mean-stdev))/SUM(Mean)")

    # Next part
    out.write("\n\nRatios of fractions Around/Whole\n")
    out.write("Category;" + ";".join([lip_list[x] for x in range(len(lip_list))]) + ";;Formula\nRatios;")
    around_per = numpy.array([around_lips[x][2] for x in lip_list])
    whole_per = numpy.array([all_lipids_per[x]/100 for x in lip_list])
    ratios = around_per/whole_per
    out.write(";".join([str(round(x, nomnom)) for x in ratios]) + ";;Fraction_around/Fraction_whole\nFluctuations;")
    fluctuations2 = []
    for i in range(len(fluctuations)):
        out.write(str(round(ratios[i]-(around_per[i]-fluctuations[i])/whole_per[i], nomnom)) + ";")
    out.write(";(Ratio-(AroundFraction-Fluctuation))/WholeFraction")

    # Next part
    out.write("\n\nTopologies – Number of beads and insaturations by lipid\n")
    out.write("Category;" + ";".join([lip_list[x] for x in range(len(lip_list))]) + "\nBeads;9;9;9;9;22;9;9;7;22\nInsaturations;1;1;2;2;2;0;0;0;4\n")

    out.close()


