#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, subprocess
from argparse import ArgumentParser

""" Uses gromacs analyze to make a bunch of autocorrelations

Usage: do_autocorr_gro.py -f vicltab.csv -o vicltab """

######################## Parsing stuff ########################

directory = ""

parser = ArgumentParser(description="""  """)

# Named arguments
parser.add_argument("-f", "--file", help="The name of the original csv file submitted in split_table.py")
parser.add_argument("-o", "--output", help="The output name, the one used in split_table.py")

# Optional arguments
parser.add_argument("-d", "--directory", help="Output directory ? Default is the current directory.")

args = parser.parse_args()

######################## Directory stuff ########################

if args.directory:
    # Checks if the directory has a /
    if args.directory[-1] == "/":
        directory += args.directory
    else:
        directory += args.directory + "/"
    subprocess.Popen("mkdir " + directory, shell=True, stderr=subprocess.PIPE).wait()

######################## Functions and miscellaneous ########################

def jump(start, end, step):
    """ Just a range with floats """
    r = start
    while r < end:
        yield r
        r += step

######################## Parser checking stuff ########################

# I actually won't check if the user is dumb
xvg_filename = args.file
output_name = args.output

# Sample gromacs command
gro_command = "gmx analyze -f %s -ac %s"

######################## Main ########################

if __name__ == '__main__':

    # Get the list of splitted files from the orignial csv - first open the file and get the lipid names
    headers = open(xvg_filename, "r").readlines()[0].split()[1:]

    # Reconstitute the list
    file_list = [output_name + "_" + x + ".xvg" for x in headers]

    # Now that we have the list of files, use gromacs analyze
    for x in file_list:
        com = gro_command % (x, x.rstrip(".xvg") + "_autocorr.xvg")
        subprocess.Popen(com, shell=True).wait()

