# Friand_Dur  Copyright Notice
# ============================
#
# Science thrives on openness. This is not a classic copyright notice.
# You may freely use, copy, modify, devastate, rape or steal any
# part of this code, with or without notification to its original owner.
#
# You MUST include a copy of this notice in all versions of any
# software that include even a tiny bit of this code. This is because
# it is science, and science needs openness, badly.
#
# ---------------------------------------------------------------------
__doc__="""
Friand_Dur is my personal nickname. It is also, coincidentially, the name
of the collection of classes I made to handle structures or trajectories
after I (finally) decided myself to stop using one-shot without classes
scripts to do everything.

Any ressemblance to existing code is purely coincidental.

Remake everything this way:
- Model, which is usually single, has a full-fledged PDB or GRO inside -- Chain is
- Chain doesn't exist.
- Atom has all the basic informations about atoms
- Residue is parallel in that it holds information about parts of the molecule
- Index has all the informations used to store index .ndx files (and reuse them)
"""
__version__ = '1'
FRIAND_DUR_VERSION = __version__

import os, sys, subprocess
from Model import *
from Atom import *
from Residue import *
from Index import *
