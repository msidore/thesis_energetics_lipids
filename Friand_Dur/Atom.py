# Friand_Dur  Copyright Notice
# ----------------------------
#
# Science thrives on openness. This is not a classic copyright notice.
# You may freely use, copy, modify, devastate, rape or steal any
# part of this code, with or without notification to its original owner.
#
# You MUST include a copy of this notice in all versions of any
# software that include even a tiny bit of this code. This is because
# it is science, and science needs openness, badly.
#
# ---------------------------------------------------------------------
__doc__="""
The Atom class.
Contains everything to handle one single atom, including reading and writing a single PDB or GRO line (writing - SOON)
"""

# No idea how to do otherwise
backbone_atoms = ["N", "C", "CA"]
CG_atoms = ["BB", "SC1", "SC2", "SC3", "SC4"]

import numpy, sys

class Atom:
    def __init__(self, **kwargs):
        self.x = None
        self.y = None
        self.z = None
        self.coords = None
        self.type = None
        self.name = None
        self.race = None
        self.charge = 0
        self.number = None
        self.altloc = None
        self.chain_id = None
        self.resname = None
        self.resnumber = None
        self.bb = False
        for key, val in kwargs.items():
            setattr(self, key, val)

    def read_PDB_line(self, line):
        """ PDB line to fill the Atom class """

        self.race=line[0:6]
        self.number=int(line[6:11])
        self.name=line[12:16].strip()
        self.altloc=line[16]
        self.resname=line[17:21].strip().upper()
        self.chain_id=line[21]
        self.resnumber=int(line[22:27])
        self.coords=numpy.array([float(line[30:38]), float(line[38:46]),float(line[46:54])])

        if self.name in backbone_atoms:
            self.bb = True

        return self

    def read_GRO_protein_line(self, line):
        """ GRO line to fill the Atom class, only with proteins """
        
        self.resnumber=int(line[0:5])
        self.resname=line[5:10].strip().upper()
        self.name=line[10:15].strip()
        # Putting the name as STRING for convenience (I know, particular case)
        # ~ self.number=int(line[15:20])
        self.number=line[15:20].strip()
        self.coords=numpy.array([float(line[20:28]), float(line[28:36]),float(line[36:44])])
        
        # Only return if it's a cg protein atom - right now only gro MARTINI atoms are here
        if self.name not in CG_atoms:
            return None        

        return self















