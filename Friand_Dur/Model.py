#!/usr/bin/python
# -*- coding: utf-8 -*-
# Friand_Dur  Copyright Notice
# ----------------------------
#
# Science thrives on openness. This is not a classic copyright notice.
# You may freely use, copy, modify, devastate, rape or steal any
# part of this code, with or without notification to its original owner.
#
# You MUST include a copy of this notice in all versions of any
# software that include even a tiny bit of this code. This is because
# it is science, and science needs openness, badly.
#
# ---------------------------------------------------------------------
__doc__="""
This module contains the top class Model.
It is the top class of the hierarchy and calls a subclass AtomHandler to handle atom lists.
"""

from Atom import *
from Residue import *
from Index import *
import numpy, sys

class Model:
    """ Class to handle a PDB or GRO file, which uses the AtomHandler class """

    def __init__(self, pdb=None, gro_protein=None, amber03_residues=False, ndx=None, **kwargs):
        """ the __init___ """

        # A few default (and empty) attributes
        self.title = 'Friandise'
        # Only one chain, so this is probably useless
        self.chains = []
        # This will be a list of Atom objects
        self.atoms = []
        # A list of atom numbers for backbone
        self.backbone = []
        self.residues = []
        self.indexes = []
        self.name = None
        self.id = 0
        # Converts all the arguments called from AtomHandler(a=A, b=B), passed into kwargs {'a': 'A', 'b': 'B'} into attributes of the class
        # Calling class.a will return A
        for key, val in kwargs.items():
            setattr(self, key, val)
            
        if ndx!=None:
            self.read_ndx(ndx)

        if pdb!=None:
            if pdb[-4:] == ".pdb":
                self.read_PDB(pdb)
            elif pdb[-4:] == ".gro":
                self.read_GRO(pdb)
            else:
                print "File containing the system not recognized, exiting ..."
                sys.exit()

        if amber03_residues!=False:
            self.compute_residues("amber03")
            
        if gro_protein!=None:
            self.read_GRO_protein(gro_protein)

    def read_ndx(self, ndx):
        """ Store a list of index names """

        # First split the file into the different indexes
        whole = open(ndx, "r").read()
        # Split with [, removing the first empty list item
        whole_l = whole.split("[")[1:]
        # Now we have each selection in a list - go through that list and put everything in the index class, which then goes into the indexes list of Model
        for index in whole_l:
            ind = Index_GRO().store_ndx(index)
            self.indexes.append(ind)

        return self
        
    def add_indx(self, name, indx):
        """ Adds an index """
        ind = Index_GRO().add_ndx(name, indx)
        self.indexes.append(ind)
        
        return self
        
    def write_ndx(self, output):
        """ Write a new index file """
        
        # The output
        out_ = open(output, "w")
        
        # The ndx line template
        title_template = "[ {:s} ] \n"
        
        # Loop through all the indexes
        for index in self.indexes:
            # The title, and write it
            name = index.get_title()
            out_.write(title_template.format(name))
            
            # And the indexes, in a list
            iis = index.get_indic()
            # And in str (?)
            iis = [str(i) for i in iis]
            
            # Now loop every 15 to write lines
            for j in range(0, len(iis), 15):
                
                # Write the line
                try:
                    line = " ".join(iis[j:j+15])
                # That's because you don't have 15 stuff
                except:
                    line = " ".join(iis[j:])
                    
                out_.write(line + " \n")
                
        out_.close()
        
        return self
            

    def get_index_titles(self):
        """ Just get a list of the different index titles """

        return [i.get_title() for i in self.indexes]
        
    def get_indices(self):
        """ Just get a list of the different indexes, same order as the titles """
        
        return [i.get_indic() for i in self.indexes]
        
    def get_all_the_indexes(self):
        """ Get a list of indtuples """
        
        return [i.get_indtuple() for i in self.indexes]

    def read_atom_list(self, atoms):
        """ Read an atom list """

        for atom in atoms:
            # Get the residue list
            res_list = [i.get_resid() for i in self.residues]
            atomresid = atom.resnumber
            for i, resid in enumerate(res_list):
                # If the residue is indeed already there, just have to add the atom
                if atomresid == resid:
                    self.residues[i].add_atom(atom)
            # If the residue of this atom isn't in that list, create a residue
            if atomresid not in res_list:
                self.residues.append(Residue(newatom=atom))

        return self

    def read_PDB(self, pdb_name):
        """ To read a PDB and fill the class with whatever's inside
        At this moment, no multiple models allowed, this will just check all the lines """

        with open(pdb_name) as openfileobject:
            for line in openfileobject:
                if line[:4]=='ATOM' or line[:6]=='HETATM':
                    a = Atom().read_PDB_line(line)
                    self.atoms.append(a)

        # Add a residue list ?
        self.residues = self.read_atom_list(self.atoms)

        return self
        
    def read_GRO_protein(self, gro_name):
        """ Read a gro and store only the protein atoms """
        
        # Excessively large default value
        number_of_atoms = 100000000
        
        with open(gro_name) as openfileobject:   
            for i, line in enumerate(openfileobject):
                # First line is header, second is number of atoms
                if i == 1: 
                    number_of_atoms = int(line.strip("\n"))
                    continue
                    
                # If we get to the box vectors ... so number of atoms +1 since we start to count at 0
                if i == number_of_atoms +1:
                    return self 
                    
                # Store the line
                if i > 1:
                    a = Atom().read_GRO_protein_line(line)
                    if a != None:
                        self.atoms.append(a)


    def get_atomname_list(self, selection="all"):
        """ To get a list of atoms """
        if selection == "all":
            return [x.name for x in self.atoms]
        elif selection == "backbone":
            return [x.name for x in self.atoms if x.bb == True]

    def get_atomnumber_list(self, selection="all"):
        """ To get a list of atoms """
        if selection == "all":
            return [x.number for x in self.atoms]
        elif selection == "backbone":
            return [x.number for x in self.atoms if x.bb == True]

    def get_coords(self, selection="all"):
        """ To simply get a list of coordinates """
        if selection == "all":
            return numpy.array([x.coords for x in self.atoms])
        elif selection == "backbone":
            return numpy.array([x.coords for x in self.atoms if x.bb == True])

    def get_all_the_atoms(self):
        return self.atoms
        
    def get_atom_from_index(self, ind):
        """Get an atom from its index number """
        
        print ind
        print "this was the ind"
        cc=0
        for atom in self.atoms:
            if atom.number == ind:
                print "found !"
                return atom
            cc+=1
        print cc
        print len(self.atoms)

        print "Couldn't find atom nr {:d}".format(ind)
        return 0
